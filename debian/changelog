quelcom (0.4.0-18) unstable; urgency=medium

  * QA upload.
  * Updated vcs in d/control to Salsa.
  * Added d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 4.6.1 to 4.7.0.

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sat, 27 Apr 2024 18:50:58 +0200

quelcom (0.4.0-17) unstable; urgency=medium

  * QA upload.
  * Drop unnecessary Depends. (Closes: #1013850)

 -- Bastian Germann <bage@debian.org>  Wed, 06 Sep 2023 00:24:45 +0200

quelcom (0.4.0-16) unstable; urgency=medium

  * QA upload.
  * debian/rules: Convert to "dh". (Closes: #931233)
  * Adjust makefiles to add EXTRA_CXXFLAGS to CXXFLAGS.
  * debian/rules: Pass CFLAGS as EXTRA_CFLAGS in dh_auto_build override.
    (Closes: #1010944)
  * debian/copyright: Reference GPL-2 in common-licenses explicitly.
  * debian/control: Update Standards-Version to 4.6.1.

 -- Vagrant Cascadian <vagrant@debian.org>  Fri, 13 May 2022 13:58:38 -0700

quelcom (0.4.0-15) unstable; urgency=medium

  * QA upload.
  * debian/control: added 'Rules-Requires-Root: no' to source stanza.
  * debian/tests/control: created to perform trivial CI tests.
  * debian/watch: created using a fake site to explain about the
    current status of the original upstream homepage.

 -- Leonardo Santiago Sidon da Rocha <leonardossr@gmail.com>  Thu, 14 May 2020 18:46:57 -0300

quelcom (0.4.0-14) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #920213)
  * Using new DH level format. Consequently:
        debian/compat: removed.
        debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
     - Bumped Standards-Version to 4.5.0.
     - Changed Priority from extra to optional.
  * debian/rules: changed from dh_clean to dh_prep in install target.

 -- Leonardo Santiago Sidon da Rocha <leonardossr@gmail.com>  Mon, 11 May 2020 14:30:15 -0300

quelcom (0.4.0-13) unstable; urgency=low

  * Convert to dpkg-source 3.0 (quilt)
  * Standards-version 3.9.2
  * Lintian fixes

 -- Devin Carraway <devin@debian.org>  Tue, 17 Jan 2012 01:09:33 -0800

quelcom (0.4.0-12) unstable; urgency=low

  * 11_info_section.patch: Specify info section in texinfo file
  * Standards-version 3.7.3
  * Use debhelper compatibility v6
  * When building, Specify LD_LIBRARY_PATH for dpkg-shlibdeps
  * 12_qwavsilence_language.patch: Fix word choice in qwavsilence.1
    manpage; thanks to A. Costa (Closes: #458992)
  * 13_gcc4.3-fixes.patch: patch in various missing #includes for gcc
    4.3 (Closes: #455144)

 -- Devin Carraway <devin@debian.org>  Thu, 10 Jan 2008 01:46:40 -0800

quelcom (0.4.0-11) unstable; urgency=low

  * 10_doc_makefile_bashism.patch: Remove a bash-specific glob pattern
  * Standards-version 3.7.2
  * Update FSF address

 -- Devin Carraway <devin@debian.org>  Sat, 29 Jul 2006 13:41:30 -0700

quelcom (0.4.0-10) unstable; urgency=low

  * Fix EINVAL errors on file creation under 2.6.12 and later kernels
    caused by new SuSv3 compliancy in mmap() handling of zero-length
    maps. (Closes: #331651)
  * changelog Fix FTBFS under old (pre-3.0) gcc/stdc++ versions where
    stdint.h isn't included by sys/types.h (Closes: #332243)

 -- Devin Carraway <devin@debian.org>  Sun,  9 Oct 2005 03:52:12 -0700

quelcom (0.4.0-9) unstable; urgency=low

  * Fix FTBFS on LP64 archs with gcc4, e.g. amd64 (Closes: #322896)

 -- Devin Carraway <devin@debian.org>  Sun, 14 Aug 2005 14:06:35 -0700

quelcom (0.4.0-8) unstable; urgency=low

  * gcc4 ABI rebuild
  * Fix FTBFS in lib/qwavsample.cc under gcc4
  * Standards-Version 3.6.2

 -- Devin Carraway <devin@debian.org>  Sun, 31 Jul 2005 19:42:16 -0700

quelcom (0.4.0-7) unstable; urgency=low

  * Standards-Version 3.6.1

 -- Devin Carraway <devin@debian.org>  Thu,  9 Sep 2004 01:35:54 -0700

quelcom (0.4.0-6) unstable; urgency=low

  * Change maintainer address
  * In case the wrapper script fails its exec, don't proceed to show the
    commandline usage.

 -- Devin Carraway <devin@debian.org>  Sat, 22 Feb 2003 00:03:03 -0800

quelcom (0.4.0-5) unstable; urgency=low

  * Don't use echo -e, for sake of POSIX/SuS.3 (Closes: #175719)
  * Move to Standards-Version 3.5.8

 -- Devin Carraway <debian@devin.com>  Wed,  8 Jan 2003 00:36:53 -0800

quelcom (0.4.0-4) unstable; urgency=low

  * Endian fixes for MP3 files (Closes: #163828)
  * Proper $@ argument quoting in wrapper script

 -- Devin Carraway <debian@devin.com>  Sun, 27 Oct 2002 16:52:55 -0800

quelcom (0.4.0-3) unstable; urgency=low

  * Compile library code as PIC (Closes: #163381)
  * Endian fixes for WAV files

 -- Devin Carraway <debian@devin.com>  Thu, 10 Oct 2002 03:46:42 -0700

quelcom (0.4.0-2) unstable; urgency=low

  * Build-depend on texinfo, gettext (Closes: Bug#163114)
  * gcc3 fixes

 -- Devin Carraway <debian@devin.com>  Wed,  2 Oct 2002 14:32:04 -0700

quelcom (0.4.0-1) unstable; urgency=low

  * Initial Debianization

 -- Devin Carraway <debian@devin.com>  Tue, 17 Sep 2002 22:32:38 -0700
