#!/bin/sh
# $Id: wrapper.sh 79 2003-02-22 09:17:33Z aqua $

if [ -z "$LD_LIBRARY_PATH" ] ; then
	LD_LIBRARY_PATH=/usr/lib/quelcom
else
	LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/quelcom
fi
export LD_LIBRARY_PATH

X=/usr/lib/quelcom/`basename "$0"`

if [ -e "$X" ]; then
	exec $X "$@"
	exit
fi

echo usage:
for e in /usr/lib/quelcom/q* ; do
	echo "        "`basename "$e"`" [...]"
done

